import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { YoyHabComponent } from './yoy-hab.component';

describe('YoyHabComponent', () => {
  let component: YoyHabComponent;
  let fixture: ComponentFixture<YoyHabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ YoyHabComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(YoyHabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
